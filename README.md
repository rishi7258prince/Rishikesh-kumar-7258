<img src="./Images/Ocean_View.png" alt="Ocean View" >

# Rishikesh Kumar
Hello, I am a **Programmer**.
I am have started my journey on the ship of **Computer Science** as a captain from the port of **Guru Ghasidas University**.
This ship is sailing towards the vast ocean of science and technology.
I am using engines of **Web Development, Game development and Artificial Intelligence** in this ship. Some engines are good some engines are basic but I am pretty sure I am going to make all the engines powerful such that when the storm comes all of them works in my favour.

## Badges
![visitors](https://visitor-badge.glitch.me/badge?page_id=Rishikesh-kumar-7258)
![Visits Badge](https://badges.pufler.dev/visits/Rishikesh-kumar-7258/Rishikesh-kumar-7258)
![Years Badge](https://badges.pufler.dev/years/Rishikesh-kumar-7258)
![Repos Badge](https://badges.pufler.dev/repos/Rishikesh-kumar-7258)
<!-- ![Gists Badge](https://badges.pufler.dev/gists/Rishikesh-kumar-7258) -->

## Stats
![Rishikesh's GitHub stats](https://github-readme-stats.vercel.app/api?username=Rishikesh-kumar-7258&count_private=true&show_icons=true&theme=tokyonight)

## Top Languages
![Top Langs](https://github-readme-stats.vercel.app/api/top-langs/?username=Rishikesh-kumar-7258&layout=compact&langs_count=15&theme=tokyonight)

## Favourite repos
![Readme Card](https://github-readme-stats.vercel.app/api/pin/?username=Rishikesh-kumar-7258&repo=Tic-Tac-Toe&theme=tokyonight)
![Readme Card](https://github-readme-stats.vercel.app/api/pin/?username=Rishikesh-kumar-7258&repo=Block_breaker&theme=tokyonight)

## My portfolio
![Readme Card](https://github-readme-stats.vercel.app/api/pin/?username=Rishikesh-kumar-7258&repo=portfolio_&theme=tokyonight)

If you want to visit this website please visit [this link](https://rishikesh-kumar-7258.github.io/portfolio_/)

**Thank you**
<!-- [![Rishikesh's wakatime stats](https://github-readme-stats.vercel.app/api/wakatime?username=Rishikesh-kumar-7258)] -->
